package com.challenge.project.vaccines.application.advice;

import com.challenge.project.vaccines.application.domain.UserException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class Advice {

    @ResponseStatus( HttpStatus.BAD_REQUEST )
    @ExceptionHandler( {MethodArgumentNotValidException.class} )
    public ErrorResponse methodsArgumentedInvalid( MethodArgumentNotValidException e ){
        return new ErrorResponse( 400, e.getMessage() );
    }

    @ResponseStatus( HttpStatus.BAD_REQUEST )
    @ExceptionHandler( {ConstraintViolationException.class} )
    public ErrorResponse valuesNotValid( ConstraintViolationException e ){
        return new ErrorResponse( 400, e.getMessage() );
    }

    @ResponseStatus( HttpStatus.BAD_REQUEST )
    @ExceptionHandler( {UserException.class} )
    public ErrorResponse userException( UserException e ){
        return new ErrorResponse( 400, e.getMessage() );
    }

}
