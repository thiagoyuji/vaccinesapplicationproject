package com.challenge.project.vaccines.application.application;

import com.challenge.project.vaccines.application.domain.User;
import com.challenge.project.vaccines.application.domain.UserOperations;

public interface UserUseCase {

    static UserUseCase create(final UserOperations userOperations ){
        return new UserUseCaseImplementation( userOperations );
    }

    User registerUser( final User user );

}
