package com.challenge.project.vaccines.application.application;

import com.challenge.project.vaccines.application.domain.User;
import com.challenge.project.vaccines.application.domain.UserOperations;

public class UserUseCaseImplementation implements UserUseCase {

    private final UserOperations userOperations;

    public UserUseCaseImplementation(final UserOperations userOperations){
        this.userOperations = userOperations;
    }

    @Override
    public User registerUser(final User user) {
        return user.registerUser( userOperations );
    }

}
