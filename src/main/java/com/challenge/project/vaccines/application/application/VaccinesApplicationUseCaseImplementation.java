package com.challenge.project.vaccines.application.application;

import com.challenge.project.vaccines.application.domain.UserOperations;
import com.challenge.project.vaccines.application.domain.VaccinesApplication;
import com.challenge.project.vaccines.application.domain.VaccinesApplicationOperations;

public class VaccinesApplicationUseCaseImplementation implements VaccinesApplicationUseCase {

    private final UserOperations userOperations;
    private final VaccinesApplicationOperations vaccinesApplicationOperations;

    public VaccinesApplicationUseCaseImplementation(UserOperations userOperations, VaccinesApplicationOperations vaccinesApplicationOperations) {
        this.userOperations = userOperations;
        this.vaccinesApplicationOperations = vaccinesApplicationOperations;
    }

    @Override
    public VaccinesApplication registerVaccinesApplication(final VaccinesApplication vaccinesApplication) {
        return vaccinesApplication.registerVaccinesApplication( vaccinesApplicationOperations, userOperations );
    }

}
