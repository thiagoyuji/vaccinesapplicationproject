package com.challenge.project.vaccines.application.controllers;

import com.challenge.project.vaccines.application.application.VaccinesApplicationUseCase;
import com.challenge.project.vaccines.application.domain.VaccinesApplication;
import com.challenge.project.vaccines.application.dto.Response;
import com.challenge.project.vaccines.application.dto.VaccinesApplicationDto;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
public class VaccinesApplicationController {

    private final VaccinesApplicationUseCase vaccinesApplicationUseCase;

    public VaccinesApplicationController(final VaccinesApplicationUseCase vaccinesApplicationUseCase) {
        this.vaccinesApplicationUseCase = vaccinesApplicationUseCase;
    }

    @PostMapping( "vaccines-application" )
    @ResponseStatus( HttpStatus.CREATED )
    public @ResponseBody Response saveUser(@RequestBody @Validated final VaccinesApplicationDto vaccinesApplicationDto ){
        return vaccinesApplicationDto.saveVaccinesApplication( vaccinesApplicationUseCase );
    }

}
