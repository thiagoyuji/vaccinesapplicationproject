package com.challenge.project.vaccines.application.domain;

import java.time.LocalDate;
import java.util.Optional;

public class User {

    public static Builder builder(){
        return new Builder();
    }

    private User( final Builder builder ){
        id = builder.id;
        name = builder.name;
        cpf = builder.cpf;
        email = builder.email;
        birthDate = builder.birthDate;
    }

    private Long id;
    private final String name;
    private final String cpf;
    private final String email;
    private final LocalDate birthDate;

    public User registerUser( final UserOperations userOperations ){
        findUserByEmailAndCpf( userOperations )
                .ifPresent( s -> { throw new UserException("User already exists !!"); } );
        return userOperations.registerUser( this );
    }

    private Optional<User> findUserByEmailAndCpf( final UserOperations userOperations ){
        return userOperations.findUserByEmailAOrCpf( email, cpf );
    }

    static public class Builder{

        private Long id;
        private String name;
        private String cpf;
        private String email;
        private LocalDate birthDate;

        public Builder withId( final Long id ){
            this.id = id;
            return this;
        }

        public Builder withName( final String name ){
            this.name = name;
            return this;
        }

        public Builder withCpf( final String cpf ){
            this.cpf = cpf;
            return this;
        }

        public Builder withEmail( final String email ){
            this.email = email;
            return this;
        }

        public Builder withBirthDate( final LocalDate birthDate ){
            this.birthDate = birthDate;
            return this;
        }

        public User build(){
            return new User( this );
        }

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCpf() {
        return cpf;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

}
