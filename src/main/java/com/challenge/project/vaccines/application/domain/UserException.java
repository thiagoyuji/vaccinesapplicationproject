package com.challenge.project.vaccines.application.domain;

public class UserException extends RuntimeException {
    UserException(String s) { super(s); }
}
