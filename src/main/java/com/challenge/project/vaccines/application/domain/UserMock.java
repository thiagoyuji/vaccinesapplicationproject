package com.challenge.project.vaccines.application.domain;

import java.time.LocalDate;

public class UserMock {

    public User toUser( final String cpf, final String email ){
        return User.builder()
                .withName("Joao")
                .withCpf( cpf )
                .withEmail( email )
                .withBirthDate( LocalDate.now() )
                .build();
    }

}
