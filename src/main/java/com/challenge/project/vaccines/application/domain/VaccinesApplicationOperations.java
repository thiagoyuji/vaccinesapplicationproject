package com.challenge.project.vaccines.application.domain;

public interface VaccinesApplicationOperations {

    VaccinesApplication registerVaccinesApplication( final VaccinesApplication vaccinesApplication );

}
