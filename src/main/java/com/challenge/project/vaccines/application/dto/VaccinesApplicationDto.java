package com.challenge.project.vaccines.application.dto;

import com.challenge.project.vaccines.application.application.VaccinesApplicationUseCase;
import com.challenge.project.vaccines.application.domain.VaccinesApplication;

import java.time.LocalDate;

public class VaccinesApplicationDto {

    private final VaccinesApplication.Builder builder;

    public VaccinesApplicationDto() {
        builder = VaccinesApplication.builder();
    }

    public void setName(final String name) {
        builder.withName( name );
    }

    public void setUserEmail(final String userEmail) {
        builder.withUserEmail( userEmail );
    }

    public void setApplicationDate(final LocalDate applicationDate) {
        builder.withApplicationDate( applicationDate );
    }

    public Response saveVaccinesApplication(final VaccinesApplicationUseCase vaccinesApplicationUseCase) {
        return new Response(
                vaccinesApplicationUseCase
                        .registerVaccinesApplication( builder.build() )
                        .getId()
        );
    }

}
