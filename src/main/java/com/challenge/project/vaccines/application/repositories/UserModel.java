package com.challenge.project.vaccines.application.repositories;

import com.challenge.project.vaccines.application.domain.User;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Table( name = "user" )
@Entity
class UserModel {

    private UserModel(){}

    UserModel( final User user ){
        name = user.getName();
        cpf = user.getCpf();
        email = user.getEmail();
        birthDate = user.getBirthDate();
    }

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Size( max = 30 )
    @Column(name = "name")
    private String name;

    @NotBlank
    @CPF
    @Size(max = 11)
    @Column(name = "cpf", columnDefinition = "char(11)")
    private String cpf;

    @NotBlank
    @Email
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "birth_date")
    private LocalDate birthDate;

    User toUser(){
        return User.builder()
                .withId( id )
                .withName( name )
                .withCpf( cpf )
                .withEmail( email )
                .withBirthDate( birthDate )
                .build();
    }

    User save(final UserRepository userRepository) {
        return userRepository.save( this ).toUser();
    }

}
