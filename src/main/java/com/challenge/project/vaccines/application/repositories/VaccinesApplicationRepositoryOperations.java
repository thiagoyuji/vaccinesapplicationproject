package com.challenge.project.vaccines.application.repositories;

import com.challenge.project.vaccines.application.domain.VaccinesApplication;
import com.challenge.project.vaccines.application.domain.VaccinesApplicationOperations;

public class VaccinesApplicationRepositoryOperations implements VaccinesApplicationOperations {

    private final VaccinesApplicationRepository vaccinesApplicationRepository;

    public VaccinesApplicationRepositoryOperations(final VaccinesApplicationRepository vaccinesApplicationRepository) {
        this.vaccinesApplicationRepository = vaccinesApplicationRepository;
    }

    @Override
    public VaccinesApplication registerVaccinesApplication(final VaccinesApplication vaccinesApplication) {
        return new VaccinesApplicationModel( vaccinesApplication ).save( vaccinesApplicationRepository );
    }

}
