package com.challenge.project.vaccines.application;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class MockError {

    public void mockBadRequest(final MockMvc mockMvc, final String request ) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .contentType( MediaType.APPLICATION_JSON )
                .content( request ))
                .andDo( MockMvcResultHandlers.print() )
                .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                .andExpect( MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE) )
                .andExpect( MockMvcResultMatchers.jsonPath("$.code").value(400) );
    }

}
