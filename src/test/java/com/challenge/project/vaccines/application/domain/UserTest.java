package com.challenge.project.vaccines.application.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UserTest {

    @Test
    @DisplayName("Register user success")
    public void registerUser(){

        final User user = new UserMock().toUser("035.988.430-03", "joao.silva@dominio.com");

        final User userReturned = user.registerUser(newUserOperations( user ));

        Assertions.assertEquals( user, userReturned);

    }

    @Test
    @DisplayName("Register user error - user already registered")
    public void registerUserAlreadyRegistered(){

        final User user = new UserMock().toUser("614.258.720-18", "joao@dominio.com");

        Assertions.assertThrows( UserException.class, () -> user.registerUser(newUserOperations( user )) );

    }

    @Test
    @DisplayName("Register user error - user cpf already registered")
    public void registerUserCpfAlreadyRegistered(){

        final User user = new UserMock().toUser("614.258.720-18", "joao.silva@dominio.com");

        Assertions.assertThrows( UserException.class, () -> user.registerUser(newUserOperations( user )) );

    }

    @Test
    @DisplayName("Register user error - user email already registered")
    public void registerUserEmailAlreadyRegistered(){

        final User user = new UserMock().toUser("035.988.430-03", "joao@dominio.com");

        Assertions.assertThrows( UserException.class, () -> user.registerUser(newUserOperations( user )) );

    }

    private UserOperations newUserOperations(final User userParameter ){
        return new UserOperationsTest().newUserOperations( userParameter );
    }

}
